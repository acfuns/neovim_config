local present, null_ls = pcall(require, "null-ls")

if not present then
  return
end

local b = null_ls.builtins

local sources = {

  b.formatting.prettier,

  -- Lua
  b.formatting.stylua,

  -- c\c++
  b.formatting.clang_format,

  -- go
  b.formatting.gofmt,
  b.formatting.goimports,

  -- rust
  b.formatting.rustfmt,

  -- toml
  b.formatting.taplo,
}

null_ls.setup {
  debug = true,
  sources = sources,
}
